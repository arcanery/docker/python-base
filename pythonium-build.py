#!/usr/bin/env python3.12

# `pythonium` - improved base container images for Python
# Copyright (C) 2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime, timezone
from typing import TypedDict
import subprocess


REGISTRY_IMAGE_NAME = "registry.gitlab.com/arcanery/containers/pythonium"
REGISTRY_LATEST_VERSION = "3.13"
REGISTRY_LATEST_KIND = "debian"

class Kind(TypedDict):
    name: str
    suffix: str
    context: str


SUPPORTED_PYTHON_VERSIONS = ["3.9", "3.10", "3.11", "3.12", "3.13"]
SUPPORTED_PYTHON_KINDS: list[Kind] = [
    {
        "name": "debian",
        "suffix": "",
        "context": "./containers/debian",
    },
    {
        "name": "debian-slim",
        "suffix": "-slim",
        "context": "./containers/debian",
    },
    {
        "name": "alpine",
        "suffix": "-alpine",
        "context": "./containers/alpine",
    },
]


ImageAnnotations = TypedDict("ImageAnnotations", {
    "com.docker.official-images.bashbrew.arch": str,
    "org.opencontainers.image.title": str,
    "org.opencontainers.image.description": str,
    "org.opencontainers.image.version": str,
    "org.opencontainers.image.created": str,
    "org.opencontainers.image.url": str,
    "org.opencontainers.image.documentation": str,
    "org.opencontainers.image.source": str,
    "org.opencontainers.image.revision": str,
    "org.opencontainers.image.vendor": str,
    "org.opencontainers.image.licenses": str,
    "org.opencontainers.image.authors": str,
})


def build_annotations(version: str, created: datetime, revision: str) -> ImageAnnotations:
    return {
        "com.docker.official-images.bashbrew.arch": "",
        "org.opencontainers.image.title": "Pythonium",
        "org.opencontainers.image.description": "Improved base container images for Python",
        "org.opencontainers.image.version": version,
        "org.opencontainers.image.created": created.isoformat(),
        "org.opencontainers.image.url": "https://gitlab.com/arcanery/containers/pythonium",
        "org.opencontainers.image.documentation": "https://arcanery.gitlab.io/containers/pythonium",
        "org.opencontainers.image.source": f"https://gitlab.com/arcanery/containers/pythonium/-/blob/{revision}/containers/debian/Containerfile",
        "org.opencontainers.image.revision": revision,
        "org.opencontainers.image.vendor": "Arcanery",
        "org.opencontainers.image.licenses": "GPL-3.0-or-later",
        "org.opencontainers.image.authors": "Artur Ciesielski <artur.ciesielski@gmail.com>",
    }


def build_images(tags: list[str], context: str, version: str, created: datetime, revision: str) -> None:
    command = [
        "podman", "build", context,
        "--format", "oci",
        "--build-arg", f"PYTHON_BASE_IMAGE_TAG={version}",
        "--pull=always",
    ]

    annotations = build_annotations(version, created, revision)
    for key, value in annotations.items():
        command.extend(("--annotation", f"{key}={value}"))

    for tag in tags:
        command.extend(("--tag", tag))

    subprocess.call(command)


def push_images(tags: list[str]) -> None:
    for tag in tags:
        subprocess.call(("podman", "push", tag))


def get_current_datetime() -> datetime:
    return datetime.now(timezone.utc)


def get_git_revision_hash() -> str:
    with open(".git/HEAD") as git_head_file:
        git_ref = git_head_file.read().strip()
        with open(f".git/{git_ref.split(" ")[1]}") as git_ref_file:
            return git_ref_file.read().strip()


if __name__ == "__main__":
    created = get_current_datetime()
    for python_version in SUPPORTED_PYTHON_VERSIONS:
        for kind in SUPPORTED_PYTHON_KINDS:
            version = f"{python_version}{kind["suffix"]}"

            tags = [
                f"{REGISTRY_IMAGE_NAME}:{version}",
                f"{REGISTRY_IMAGE_NAME}:{version}-{int(created.timestamp())}",
            ]
            if version == REGISTRY_LATEST_VERSION and kind["name"] == REGISTRY_LATEST_KIND:
                tags.append(f"{REGISTRY_IMAGE_NAME}:latest",)

            build_images(tags, kind["context"], version, created, get_git_revision_hash())
            push_images(tags)
