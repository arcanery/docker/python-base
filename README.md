<!-- `pythonium` - improved base container images for Python
Copyright (C) 2025 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

# Python base images

## Using the image

This image is a basic wrapper layer on top of the standard, official Python image.

The repo exposes three editions of each tag based on the official Python repository on Docker Hub:
- `X.Y` (Debian)
- `X.Y-slim` (Debian)
- `X.Y-alpine` (Alpine)

Currently supported versions of Python:
- `3.9`
- `3.10`
- `3.11`
- `3.12`
- `3.13`

It adds three things of note:
- scripts that will properly update the system with newest packages and install any extra system packages, then clean up (`system-update.sh` and `system-install.sh`):
  - those scripts ensure there are no inconsistent layers in the resulting image and clean up after themselves to minimize the final image size
  - to update system packages add `RUN system-update.sh` to your Containerfile
  - to install e.g. `git` and `curl` add `RUN system-install.sh git curl` to your Containerfile
- a pre-made virtual environment, which is always active in the container
- a pre-made user for the application to use when deployed
  - in your derived image put `USER app` in the Containerfile when you're ready to drop privileges
  - the `app` user can install Python packages in the virtual environment and has a separate home directory

## Building the images

To rebuild all kinds of images in all currently supported Python versions locally, run:

```bash
./pythonium-build.py
```

This script requires Python 3.12 and `podman` to be installed on the machine, but no extra Python libraries.

## Example `Containerfile` using Pythonium

```Containerfile
FROM registry.gitlab.com/arcanery/containers/pythonium:3.12

# update system packages and install git + curl
RUN system-install.sh git curl

# drop privileges
USER app

# install Python packages in the virtual environment
RUN --mount=type=bind,target=/requirements.txt,source=requirements.txt \
  pip install -r /requirements.txt

ENV FLASK_APP="package.app"
EXPOSE 8000

ENTRYPOINT [ "python" ]
CMD [ "-m", "flask", "run" ]
```
